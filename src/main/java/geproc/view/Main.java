package geproc.view;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class Main extends Application {

    String versao = "0.0.9";

    public static String ip = "172.19.212.95";
    /*public static String ip = "localhost";*/

    @Override
    public void start(Stage primaryStage) throws Exception {
        if (getParameters().getNamed().containsKey("ip")) {
            ip = getParameters().getNamed().get("ip");
        }

        Parent root = FXMLLoader.load(getClass().getResource("/janelaPrincipal.fxml"));
        primaryStage.setTitle("GEPROC | Gerenciador de Processos " + versao);
        primaryStage.setScene(new Scene(root));
        primaryStage.setMaximized(true);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}