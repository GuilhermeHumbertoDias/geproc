package geproc.controller;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import geproc.DAO.Bd;
import geproc.model.Cadastro;
import geproc.model.Gerencia;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;
import javafx.scene.image.Image;
import java.time.LocalDate;

public class CadastrarController {

    @FXML
    public ComboBox<Gerencia> gerenciaComboBox;
    @FXML
    public TextField referenciaTextField;
    @FXML
    public TextField processoTextField;
    @FXML
    public TextField interessadoTextField;
    @FXML
    public DatePicker getigData;
    @FXML
    public DatePicker envioData;
    @FXML
    public DatePicker acervoData;
    @FXML
    public Button adicionarButton;
    @FXML
    public Button cancelarButton;
    @FXML
    public TextField paginasTextField;

    public PrincipalController principalController;

    private Cadastro cadastro;

    public void setCadastro(Cadastro cadastro) {
        this.cadastro = cadastro;
        preCadastro();
    }

    public void preCadastro() {
        gerenciaComboBox.setValue(cadastro.getGerencia());
        referenciaTextField.setText(cadastro.getReferencia());
        processoTextField.setText(cadastro.getProcesso());
        interessadoTextField.setText(cadastro.getInteressado());
        getigData.setValue(cadastro.getGetig());
        envioData.setValue(cadastro.getEnvio());
        acervoData.setValue(cadastro.getAcervo());
        paginasTextField.setText(String.valueOf(cadastro.getQuantidadePaginas()));
    }

    Bd bd = new Bd();

    @FXML
    public void initialize() {
        gerenciaComboBox.setItems(gerencias);
        gerencias.addAll(bd.getGerencias());
    }

    public void setPrincipalController(PrincipalController principal) {

        principalController = principal;
    }

    Gerencia gerencia = new Gerencia();

    ObservableList<Gerencia> gerencias = FXCollections.observableArrayList();

    @FXML
    public void adicionarMaterial() {

        if (!validarDados()) {
            exibirMensagemErro("Por favor, preencha todos os dados!");
            return;
        }

        boolean editando = true;

        if (cadastro == null) {
            cadastro = new Cadastro();
            editando = false;
        }

        Gerencia gerencia = cadastro.setGerencia(gerenciaComboBox.getValue());
        String referencia = cadastro.setReferencia(referenciaTextField.getText());
        String processo = cadastro.setProcesso(processoTextField.getText());
        String interessado = cadastro.setInteressado(interessadoTextField.getText());

        LocalDate getig = cadastro.setGetig(getigData.getValue());
        LocalDate envio = cadastro.setEnvio(envioData.getValue());
        LocalDate acervo = cadastro.setAcervo(acervoData.getValue());
        cadastro.setQuantidadePaginas(Integer.parseInt(paginasTextField.getText()));


        if (editando == false) {
            principalController.adicionarMaterial(cadastro);
            processoTextField.clear();
            interessadoTextField.clear();
            paginasTextField.clear();
            cadastro = null;
        } else {
            principalController.atualizarIntens();
            bd.atualizarCadastro(cadastro);
        }

        notificandoCadastro();
    }

    public boolean validarDados() {
        if (gerenciaComboBox.getValue() == null) {
            return false;
        }

        if (referenciaTextField.getText() == null || referenciaTextField.getText().isEmpty()) {
            return false;
        }

        if (processoTextField.getText() == null || processoTextField.getText().isEmpty()) {
            return false;
        }

        if (interessadoTextField.getText() == null || interessadoTextField.getText().isEmpty()) {
            return false;
        }

        if (getigData.getValue() == null) {
            return false;
        }

        if (envioData.getValue() == null) {
            return false;
        }

        if (acervoData.getValue() == null) {
            return false;
        }

        return true;
    }

    private void exibirMensagemErro(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erro");
        alert.setContentText(msg);
        alert.showAndWait();
    }

    public void notificandoCadastro() {
        Image img = new Image("/concluido.png");
        Notifications notifications = Notifications.create()
                .title("Cadastro Concluído")
                .text("O cadastro do processo foi concluído")
                .graphic(new ImageView(img))
                .hideAfter(Duration.seconds(2))
                .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println("Notificação cadastro");
                    }
                });
                notifications.show();
    }



    @FXML
    public void cancelarCadastro() {
        cancelarButton.getScene().getWindow().hide();
    }
}