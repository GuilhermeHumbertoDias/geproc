package geproc.controller;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import geproc.DAO.Bd;
import geproc.DAO.Conexao;
import geproc.model.Cadastro;
import geproc.model.Gerencia;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.awt.*;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class ImprimirController {

    @FXML public ComboBox<Gerencia> setorComboBox;
    @FXML
    public ComboBox<String> situacaoComboBox;
    @FXML
    public DatePicker dataDatePicker;
    @FXML
    public TextField deTextField;
    @FXML
    public TextField ateTextField;
    @FXML
    public Button imprimirButton;
    @FXML
    public Button cancelarButton;

    @FXML
    public void initialize() {
        setorComboBox.setItems(gerencias);
        gerencias.addAll(bd.getGerencias());
        situacaoComboBox.setItems(situacao);
    }

    ObservableList<Gerencia> gerencias = FXCollections.observableArrayList();

    ObservableList<String> situacao = FXCollections.observableArrayList(
            "DIGITALIZAR E DEVOLVER AO IPREV",
            "DIGITALIZAR E ARQUIVAR",
            "DIGITALIZAR E DESCARTAR"
    );

    public DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");

    Bd bd = new Bd();

    @FXML
    public void imprimirMaterial() {

        if (!validarDados()) {
            exibirMensagemErro("Por favor, preencha todos os dados!");
            return;
        }

        Document document = null;
        OutputStream outputStream = null;

        File pdf = new File("Table.pdf");

        try {
            document = new Document(PageSize.LETTER, 30, 30, 30, 30);
            outputStream = new FileOutputStream(pdf);
            try {
                PdfWriter.getInstance(document, outputStream);
                document.open();
                Paragraph paragrafo = new Paragraph("COLETA ACERVO\n\n");
                paragrafo.setAlignment(Element.ALIGN_CENTER);
                document.add(paragrafo);

                if (dataDatePicker.getValue() != null) {
                    Paragraph data = new Paragraph("DATA: " +
                            dateTimeFormatter.format(dataDatePicker.getValue()));
                    document.add(data);
                } else {
                    Paragraph data = new Paragraph("DATA: ____/____/____ ");
                    document.add(data);
                }

                Paragraph espaco = new Paragraph(" ");
                document.add(espaco);

                Paragraph assinatura = new Paragraph(
                        "Assinatura: ___________________________________");
                document.add(assinatura);
                document.add(espaco);

                Paragraph setor = new Paragraph("SETOR: " +
                        setorComboBox.getValue());
                Paragraph situacao = new Paragraph("SITUAÇÃO: "
                        + situacaoComboBox.getValue().toString());
                document.add(setor);
                document.add(situacao);
                document.add(espaco);

                PdfPTable tabela = new PdfPTable(5);
                PdfPCell cabecalho = new PdfPCell(new Paragraph("RELAÇÃO DE PROCESSOS PARA DIGITALIZAR"));
                PdfPCell numero = new PdfPCell(new Paragraph("N°"));
                PdfPCell referencia = new PdfPCell(new Paragraph("REFERÊNCIA"));
                PdfPCell processo = new PdfPCell(new Paragraph("PROCESSO"));
                PdfPCell interessado = new PdfPCell(new Paragraph("INTERESSADO"));
                PdfPCell paginas = new PdfPCell(new Paragraph("PÁGINAS"));

                tabela.setTotalWidth(580);
                tabela.setHeaderRows(2);
                tabela.setWidths(new float[]{
                        0.13f, 0.13f, 0.13f, 0.13f, 0.13f
                });
                tabela.setLockedWidth(true);

                cabecalho.setHorizontalAlignment(Element.ALIGN_CENTER);
                numero.setHorizontalAlignment(Element.ALIGN_CENTER);
                referencia.setHorizontalAlignment(Element.ALIGN_CENTER);
                processo.setHorizontalAlignment(Element.ALIGN_CENTER);
                interessado.setHorizontalAlignment(Element.ALIGN_CENTER);
                paginas.setHorizontalAlignment(Element.ALIGN_CENTER);

                tabela.setWidths(new float[]{
                        0.07f, 0.14f, 0.14f, 0.5f, 0.10f
                });

                cabecalho.setColspan(5);
                numero.getMaxHeight();
                referencia.getMaxHeight();
                processo.getMaxHeight();
                interessado.getMaxHeight();
                paginas.getMaxHeight();
                tabela.addCell(cabecalho);
                tabela.addCell(numero);
                tabela.addCell(referencia);
                tabela.addCell(processo);
                tabela.addCell(interessado);
                tabela.addCell(paginas);

                List<Cadastro> cadastros = pegarItens();

                for (int i = 0; i < cadastros.size(); i++) {
                    Cadastro cadastro = cadastros.get(i);
                    tabela.addCell(String.valueOf(i + 1));
                    tabela.addCell(cadastro.getReferencia());
                    tabela.addCell(cadastro.getProcesso());
                    tabela.addCell(cadastro.getInteressado());
                    tabela.addCell(String.valueOf(cadastro.getQuantidadePaginas()));
                    /*bd.atualizarTipo(cadastro);*/
                }
                inserindoPaginas();
                document.add(tabela);
                document.close();
                abrirArquivo(pdf);
                principalController.atualizarTabela();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }



    }

    @FXML
    public void cancelarImpressao() {
        cancelarButton.getScene().getWindow().hide();
    }

    public List<Cadastro> pegarItens() {
        int de = Integer.parseInt(deTextField.getText());
        int ate = Integer.parseInt(ateTextField.getText());

        List<Cadastro> cadastros = principalController.pegarItens();

        List<Cadastro> retorno = new ArrayList<>();

        for (int i = de - 1; i < ate; i++) {
            retorno.add(cadastros.get(i));
        }

        return retorno;
    }

    public PrincipalController principalController;

    public void setPrincipalController(PrincipalController principal) {
        principalController = principal;
    }

    public void abrirArquivo(File file) {
        String SO = System.getProperty("os.name").toLowerCase();
        Runtime r = Runtime.getRuntime();

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    if (Desktop.isDesktopSupported()) {
                        Desktop.getDesktop().open(file);
                    } else if (SO.indexOf("linux") >= 0) {
                        r.exec("xdg-open " + file.getAbsolutePath());
                    } else {
                        r.exec("rundll32 url.dll,FileProtocolHandler "
                                + file.getAbsolutePath());
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }

    private void exibirMensagemErro(String msg) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Erro");
        alert.setContentText(msg);
        alert.showAndWait();
    }

    public boolean validarDados() {
        if (setorComboBox.getValue() == null) {
            return false;
        }

        if (situacaoComboBox.getValue() == null) {
            return false;
        }

        if (deTextField.getText() == null || deTextField.getText().isEmpty()) {
            return false;
        }

        if (ateTextField.getText() == null || ateTextField.getText().isEmpty()) {
            return false;
        }
        return true;
    }

    public void pegarPaginas() {



    }

    public void inserindoPaginas() {


        Conexao con = new Conexao();

        LocalDate data = LocalDate.now();

        String sql = "INSERT into paginas_acervo (gerencia, data_impressao, total_paginas)" +
                "VALUES ('" + setorComboBox.getValue().getId() + "', '"
                + LocalDate.now() + "', '"
                + principalController.totalPaginasTextField.getText() + "');";

        System.out.println(sql);

        int res = con.ExecutaSQL(sql);

        if (res != 0) {

        } else {

        }

        con.fecharConexao();
    }

}