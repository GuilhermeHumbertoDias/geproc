package geproc.controller;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;

public class EditarController {

    @FXML
    public ComboBox gerenciaComboBox;
    @FXML
    public TextField referenciaTextField;
    @FXML
    public TextField processoTextField;
    @FXML
    public TextField interessadoTextField;
    @FXML
    public DatePicker getigData;
    @FXML
    public DatePicker envioData;
    @FXML
    public DatePicker acervoData;
    @FXML
    public Button atualizarButton;
    @FXML
    public Button cancelarButton;


    @FXML
    public void initialize() {

        gerenciaComboBox.setItems(gerencias);
    }

    ObservableList<String> gerencias = FXCollections.observableArrayList(
            "GABINETE", "GEPES", "GEAFC", "GETIG", "GEAPO", "GEPLA", "GEBEN",
            "GEINV", "GEFIS", "GECAD", "GECOJ", "GERIN", "GEPEN", "GEACP"
    );

    @FXML
    public void atualizarMaterial() {

    }

    @FXML
    public void cancelarMaterial() {

        cancelarButton.getScene().getWindow().hide();
    }

}