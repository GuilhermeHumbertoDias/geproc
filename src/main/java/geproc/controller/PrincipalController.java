package geproc.controller;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import geproc.DAO.Bd;
import geproc.DAO.Filtro;
import geproc.model.Cadastro;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.paint.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.controlsfx.control.Notifications;


import java.awt.*;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class PrincipalController {

    @FXML public Button cadastrarButton;
    @FXML public Button editarButton;
    @FXML public Button excluirButton;
    @FXML public Button imprimirButton;
    @FXML public Button filtrarButton, enviadoButton;
    @FXML public TableView<Cadastro> materiaisTableView;
    @FXML public TableColumn<Cadastro, String> gerenciaTableColumn;
    @FXML public TableColumn<Cadastro, String> referenciaTableColumn;
    @FXML public TableColumn<Cadastro, String> processoTableColumn;
    @FXML public TableColumn<Cadastro, String> interessadoTableColumn;
    @FXML public TableColumn<Cadastro, String> getigTableColumn;
    @FXML public TableColumn<Cadastro, String> enviadoTableColumn;
    @FXML public TableColumn<Cadastro, String> acervoTableColumn;
    @FXML public TableColumn<Cadastro, String> numeracaoTableColumn;
    @FXML public TableColumn<Cadastro, String> paginasTableColumn;
    @FXML public CheckMenuItem enviadosMenuItem;
    @FXML public CheckMenuItem naoMenuItem;
    @FXML public MenuItem sobreMenuItem;
    @FXML public Button definirButton;
    @FXML public Region regionSpace;
    @FXML public Button filtroButton;
    @FXML public ComboBox<Filtro> filtroComboBox;
    @FXML public TextField filtroTextField;
    @FXML public TextField totalPaginasTextField, totalMesTextField, restanteTextField;
    @FXML public Label mensagemLabel;


    public DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("dd/MM/YYYY");

    Cadastro cadastro = new Cadastro();

    @FXML
    public void initialize() {

        numeracaoTableColumn.setCellFactory(column -> new TableCell<Cadastro, String>() {
            public void updateItem(String item, boolean empty) {
                super.updateItem(item, empty);
                String index = empty ? null : getIndex() + 1 + "";
                getStyleClass().addAll("column-header");

                setGraphic(null);
                setText(index);
            }
        });

        numeracaoTableColumn.setMaxWidth(35);
        numeracaoTableColumn.setMinWidth(35);

        referenciaTableColumn.setCellValueFactory(param -> {
            return new SimpleStringProperty(param.getValue().getReferencia());
        });

        processoTableColumn.setCellValueFactory(param -> {
            return new SimpleStringProperty(param.getValue().getProcesso());
        });

        interessadoTableColumn.setCellValueFactory(param -> {
            return new SimpleStringProperty(param.getValue().getInteressado());
        });

        getigTableColumn.setCellValueFactory(param -> {
            return new SimpleStringProperty(dateTimeFormatter.format(param.getValue().getGetig()));
        });

        enviadoTableColumn.setCellValueFactory(param -> {
            return new SimpleStringProperty(dateTimeFormatter.format(param.getValue().getEnvio()));
        });

        acervoTableColumn.setCellValueFactory(param -> {
            return new SimpleStringProperty(dateTimeFormatter.format(param.getValue().getAcervo()));
        });

        gerenciaTableColumn.setCellValueFactory(param -> {
            return new SimpleStringProperty(param.getValue().getGerencia().getNome());
        });

        paginasTableColumn.setCellValueFactory(param -> {
            return new SimpleObjectProperty(param.getValue().getQuantidadePaginas());
        });


        Image cadastrarIcone = new Image(getClass().getResourceAsStream("/adicionar.png"));
        Image editarIcone = new Image(getClass().getResourceAsStream("/editar.png"));
        Image excluirIcone = new Image(getClass().getResourceAsStream("/excluir.png"));
        Image imprimirIcone = new Image(getClass().getResourceAsStream("/imprimir.png"));
        Image definirIcone = new Image(getClass().getResourceAsStream("/seta.png"));
        Image checkIcone = new Image(getClass().getResourceAsStream("/correct.png"));

        cadastrarButton.setGraphic(new ImageView(cadastrarIcone));
        editarButton.setGraphic(new ImageView(editarIcone));
        excluirButton.setGraphic(new ImageView(excluirIcone));
        imprimirButton.setGraphic(new ImageView(imprimirIcone));
        definirButton.setGraphic(new ImageView(definirIcone));
        enviadoButton.setGraphic(new ImageView(checkIcone));

        enviadosMenuItem.setSelected(false);
        naoMenuItem.setSelected(true);

        atualizarTabela();

        materiaisTableView.getSelectionModel().setSelectionMode(
                SelectionMode.MULTIPLE
        );

        HBox.setHgrow(regionSpace, Priority.ALWAYS);

        filtroComboBox.getItems().addAll(Filtro.values());
        filtroComboBox.getSelectionModel().select(Filtro.PROCESSO);
    }

    Bd bd = new Bd();
    CadastrarController cc = new CadastrarController();

    public void adicionarMaterial(Cadastro cadastro) {

        if (cadastro.getId() == 0) {
            materiaisTableView.getItems().add(cadastro);
            bd.inserindoCadastro(cadastro);
            contarPaginas();
        } else {
            contarPaginas();
    }
    }

    public void cadastrarMaterial() throws IOException {
        Stage secondStage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/janelaCadastrar.fxml"));
        Parent root = loader.load();

        CadastrarController controller = loader.getController();
        controller.setPrincipalController(this);
        secondStage.setTitle("GEPROC | CADASTRAR MATERIAL");
        secondStage.setScene(new Scene(root));
        secondStage.show();
    }

    public void atualizarIntens() {
        materiaisTableView.refresh();
        contarPaginas();
    }

    public void editarMateria() throws IOException {
        Stage secondStage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/janelaCadastrar.fxml"));
        Parent root = loader.load();

        CadastrarController controller = loader.getController();
        controller.setPrincipalController(this);
        controller.setCadastro(materiaisTableView.getSelectionModel().getSelectedItem());
        secondStage.setTitle("GEPROC | EDITAR MATERIAL");
        secondStage.setScene(new Scene(root));
        secondStage.show();
    }

    @FXML
    public void editarMaterial() throws IOException {
        Stage thirdStage = new Stage();
        Parent root = FXMLLoader.load(getClass().getResource("/janelaEditar.fxml"));
        thirdStage.setTitle("GEPROC | EDITAR MATERIAL");
        thirdStage.setScene(new Scene(root));
        thirdStage.show();
    }

    @FXML
    public void excluirMaterial() {
        ObservableList<Cadastro> cadastroSelected, todosCadastros;
        todosCadastros = materiaisTableView.getItems();
        cadastroSelected = materiaisTableView.getSelectionModel().getSelectedItems();

        cadastroSelected.forEach(bd::excluirCadastro);
        todosCadastros.removeAll(cadastroSelected);
        contarPaginas();
        notificacaoExcluir();
    }

    public void notificacaoExcluir() {
        Image img = new Image("/excluido.png");
        Notifications notifications = Notifications.create()
                .title("Cadastro Excluído")
                .text("O processo cadastrado foi excluido")
                .graphic(new ImageView(img))
                .hideAfter(Duration.seconds(2))
                .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println("Notificação cadastro");
                    }
                });
        notifications.show();
    }

    @FXML
    public void imprimirMaterial() throws IOException {
        Stage fourthStage = new Stage();
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("/janelaImprimir.fxml"));
        Parent root = loader.load();

        ImprimirController imprimirController = loader.getController();
        imprimirController.setPrincipalController(this);
        fourthStage.setTitle("GEPROC | IMPRIMIR MATERIAL");
        fourthStage.setScene(new Scene(root));
        fourthStage.show();
    }

    @FXML
    public void sobreSistema() {
        mostrarMensagem();
    }

    public void mostrarMensagem() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setTitle("Sobre o sistema");
        alert.setHeaderText("Gerenciador de Processos IPREV 2017");
        alert.setContentText(
                "Versão do sistema: 0.0.7\n" +
                "Desenvolvido por: Guilherme Humberto Dias\n" +
                "Instituto de Previdência do Estado de Santa Catarina ");
        alert.showAndWait();
    }

    public List<Cadastro> pegarItens() {
        contarPaginas();
        return materiaisTableView.getItems();
    }

    public void atualizarTabela() {
        List<Cadastro> cadastros = bd.listarMateriais(
                enviadosMenuItem.isSelected(),
                naoMenuItem.isSelected());
                materiaisTableView.getItems().clear();
                materiaisTableView.getItems().addAll(cadastros);
        contarPaginas();
    }

    @FXML
    public void definirData() {
        ObservableList<Cadastro> cadastroSelected, todosCadastros;
        todosCadastros = materiaisTableView.getItems();
        cadastroSelected = materiaisTableView.getSelectionModel().getSelectedItems();

        if (cadastroSelected.size() < 2) {
            System.out.println("Selecione mais de um processo para definir");
            return;
        }

        LocalDate data = cadastroSelected.get(0).getEnvio();

        for (Cadastro cadastro : cadastroSelected ) {
            cadastro.setEnvio(data);
            cadastro.setAcervo(data);
            bd.atualizarCadastro(cadastro);
        }

        materiaisTableView.refresh();
        notificandoCadastro();
    }

    public void notificandoCadastro() {
        Image img = new Image("/concluido.png");
        Notifications notifications = Notifications.create()
                .title("Definição de data")
                .text("As datas foram modificadas com sucesso!")
                .graphic(new ImageView(img))
                .hideAfter(Duration.seconds(2))
                .position(Pos.TOP_RIGHT)
                .onAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        System.out.println("Notificação cadastro");
                    }
                });
        notifications.show();
    }

    @FXML
    public void filtrarProcesso()  {

        String texto = filtroTextField.getText();

        Filtro filtro = filtroComboBox.getValue();

        List<Cadastro> lista = bd.filtrarMateriais(filtro, texto);

        materiaisTableView.getItems().clear();
        materiaisTableView.getItems().addAll(lista);
        contarPaginas();
        filtroTextField.clear();


    }

    int paginasTotal = 80000;

    public void contarPaginas() {

        int quantidadePaginas = 0;
        int quantidadePaginasMes = 0;

        for (Cadastro cadastro : materiaisTableView.getItems()) {
            quantidadePaginas += cadastro.getQuantidadePaginas();
        }

        totalPaginasTextField.setText(String.valueOf(quantidadePaginas));

        quantidadePaginasMes += quantidadePaginas;

        totalMesTextField.setText(String.valueOf(paginasTotal));

        int restante = paginasTotal - quantidadePaginas;

        restanteTextField.setText(String.valueOf(restante));

        if (restante <= 100) {
            restanteTextField.setStyle("-fx-text-fill: red");
        } else if (restante > 100){
            restanteTextField.setStyle("-fx-text-fill: black");
        }

        if (restante < 0) {
            mensagemLabel.setText("O limite de páginas foi atingido!");
        } else {
            mensagemLabel.setText("");
        }
    }

    @FXML public void processoEnviado() {
        /*List<Cadastro> cadastros = pegarItens();

        for (int i = 0; i < cadastros.size(); i++) {
            Cadastro cadast = cadastros.get(i);
            bd.atualizarTipo(cadast);
        }*/
        ObservableList<Cadastro> cadastroSelected, todosCadastros;
        todosCadastros = materiaisTableView.getItems();
        cadastroSelected = materiaisTableView.getSelectionModel().getSelectedItems();

        cadastroSelected.forEach(bd::atualizarTipo);
        /*bd.atualizarTipo(cadastro);*/
        atualizarTabela();
    }

    public void notificacaoLimitePaginas() {
        Alert alert = new Alert(Alert.AlertType.INFORMATION);
        alert.setHeaderText("Limite de Páginas atingido!");
        alert.setTitle("Limite de Páginas");
        alert.setContentText("O limite de páginas por mês foi atingido!");
        alert.showAndWait();
    }



}