package geproc.DAO;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import geproc.controller.ImprimirController;
import geproc.controller.PrincipalController;
import geproc.model.Cadastro;
import geproc.model.Gerencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Bd {

    Conexao con;

    Cadastro cadastro = new Cadastro();

    //Gerencia gerencia = new Gerencia();

    public void inserindoCadastro(Cadastro cadastro) {

        Conexao con = new Conexao();
        String sql = "INSERT into materiais (id_gerencia, referencia, processo, interessado, data_getig, data_entrega, data_acervo, enviado, paginas) " +
                "values ('" + cadastro.getGerencia().getId() + "', '" + cadastro.getReferencia() + "', '" + cadastro.getProcesso() + "'," +
                " '" + cadastro.getInteressado() + "', '" + cadastro.getGetig() + "', '" + cadastro.getEnvio() + "', '" + cadastro.getAcervo() + "', false " +  ", '" + cadastro.getQuantidadePaginas() + "') returning id";

        System.out.println(sql);
        ResultSet res = con.ExecutaSelect(sql);

        if (res != null) {
            try {
                if (res.next()) {
                    int id = res.getInt("id");
                    cadastro.setId(id);
                } else {

                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else {

        }

        con.fecharConexao();
    }

    public void contPaginas() {



    }

    public void inserindoPaginas() {
        Conexao con = new Conexao();
        ImprimirController imprimir = new ImprimirController();
        PrincipalController principal = new PrincipalController();
        LocalDate data = LocalDate.now();

        String sql = "INSERT into paginas_acervo (gerencia, data_impressao, total_paginas)" +
                     "VALUES ('" + imprimir.setorComboBox.getValue().getId() + "', '"
                + LocalDate.now() + "', '"
                + principal.totalPaginasTextField.getText() + "');";

        System.out.println(sql);

        int res = con.ExecutaSQL(sql);

        if (res != 0) {

        } else {

        }

        con.fecharConexao();
    }

    public List<Gerencia> getGerencias() {

        Conexao con = new Conexao();

        List<Gerencia> gerencias = new ArrayList<Gerencia>();

        String sql = "SELECT * from gerencia";

        ResultSet resultSet = con.ExecutaSelect(sql);

        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String nome = resultSet.getString("nome");
                    Gerencia gerencia = new Gerencia();
                    gerencia.setId(id);
                    gerencia.setNome(nome);
                    gerencias.add(gerencia);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return gerencias;
    }

    public void atualizarCadastro(Cadastro cadastro) {

        Conexao con = new Conexao();

        String sql = "UPDATE materiais set id_gerencia = '" + cadastro.getGerencia().getId() + "' , referencia = '" + cadastro.getReferencia() + "', " +
                "processo = '" + cadastro.getProcesso() + "', interessado = '" + cadastro.getInteressado() + "', " +
                "data_getig = '" + cadastro.getGetig() + "', data_entrega = '" + cadastro.getEnvio() + "', data_acervo = '" + cadastro.getAcervo() + "', paginas =  '" + cadastro.getQuantidadePaginas() + "' " +
                "WHERE id = '" + cadastro.getId() + "'";

        int res = con.ExecutaSQL(sql);

        if (res != 0) {

        } else {

        }

        con.fecharConexao();


    }

    public boolean excluirCadastro(Cadastro cadastro) {

        Conexao con = new Conexao();

        String sql = "DELETE from materiais WHERE id = '" + cadastro.getId() + "'";

        con.ExecutaSQL(sql);

        int res = con.ExecutaSQL(sql);

        con.fecharConexao();

        if (res != 0) {
            return true;
        } else {
            return false;
        }

    }

    public boolean atualizarDatas(Cadastro cadastro) {

        Conexao con = new Conexao();

        String sql = "UPDATE materiais set id_gerencia = '" + cadastro.getGerencia().getId() + "' , referencia = '" + cadastro.getReferencia() + "', " +
                "processo = '" + cadastro.getProcesso() + "', interessado = '" + cadastro.getInteressado() + "', " +
                "data_getig = '" + cadastro.getGetig() + "', data_entrega = '" + cadastro.getEnvio() + "', data_acervo = '" + cadastro.getAcervo() +   "', enviado = '' " + "', paginas = '" + cadastro.getQuantidadePaginas() + "' " +
                "WHERE id = '" + cadastro.getId() + "'";

        con.ExecutaSQL(sql);

        int res = con.ExecutaSQL(sql);

        con.fecharConexao();

        if (res != 0) {
            return true;
        } else {
            return false;
        }
    }

    public List<Cadastro> listarMateriais(boolean enviados, boolean naoEnviados) {
        Conexao con = new Conexao();

        String sql = "SELECT * from materiais ORDER BY id";

        if (enviados && !naoEnviados) {
            sql = "SELECT * from materiais WHERE enviado = true ORDER BY id";
        } else if (naoEnviados && !enviados) {
            sql = "SELECT * from materiais WHERE enviado = false ORDER BY id";
        }

        List<Cadastro> cadastros = new ArrayList<Cadastro>();

        List<Gerencia> gerencias = getGerencias();
        Map<Integer, Gerencia> map = new HashMap<>();

        for (Gerencia gerencia : gerencias) {
            map.put(gerencia.getId(), gerencia);
        }

        ResultSet resultSet = con.ExecutaSelect(sql);

        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String referencia = resultSet.getString("referencia");
                    String processo = resultSet.getString("processo");
                    String interessado = resultSet.getString("interessado");
                    int gerencia = resultSet.getInt("id_gerencia");
                    LocalDate getig = resultSet.getDate("data_getig").toLocalDate();
                    LocalDate envio = resultSet.getDate("data_entrega").toLocalDate();
                    LocalDate acervo = resultSet.getDate("data_acervo").toLocalDate();
                    int paginas = resultSet.getInt("paginas");
                    Cadastro cadastro = new Cadastro();
                    cadastro.setGerencia(map.get(gerencia));
                    cadastro.setId(id);
                    cadastro.setReferencia(referencia);
                    cadastro.setProcesso(processo);
                    cadastro.setInteressado(interessado);
                    cadastro.setGetig(getig);
                    cadastro.setEnvio(envio);
                    cadastro.setAcervo(acervo);
                    cadastro.setQuantidadePaginas(paginas);
                    cadastros.add(cadastro);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        con.fecharConexao();
        return cadastros;
    }


    /*public int contagemPaginasMes() {

        Conexao con = new Conexao();

        LocalDate incioMes = LocalDate.now().withDayOfMonth(1);
        LocalDate fimMes = LocalDate.now().plusMonths(1).withDayOfMonth(1);

        DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        String sql = "SELECT * from materiais WHERE enviado = true AND ORDER BY id";

        ResultSet resultSet = con.ExecutaSelect(sql);

        if (resultSet != null) {
            try {
                if (resultSet.next()) {


                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }

    }*/

    public Gerencia getGerenciaPorId(int id) {
        return null;
    }

    public void atualizarTipo(Cadastro cadastro) {

        Conexao con = new Conexao();

        String sql = "UPDATE materiais set enviado = true " +
                     "WHERE id = '" + cadastro.getId() + "'";

        int res = con.ExecutaSQL(sql);

        con.fecharConexao();

        if (res != 0) {

        } else {

        }

    }

    public List<Cadastro> listarEnviados() {
        Conexao con = new Conexao();

        String sql = "SELECT * from materiais WHERE enviado = true ORDER BY id";
        List<Cadastro> cadastros = new ArrayList<Cadastro>();

        List<Gerencia> gerencias = getGerencias();
        Map<Integer, Gerencia> map = new HashMap<>();

        for (Gerencia gerencia : gerencias) {
            map.put(gerencia.getId(), gerencia);
        }

        ResultSet resultSet = con.ExecutaSelect(sql);

        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String referencia = resultSet.getString("referencia");
                    String processo = resultSet.getString("processo");
                    String interessado = resultSet.getString("interessado");
                    int gerencia = resultSet.getInt("id_gerencia");
                    LocalDate getig = resultSet.getDate("data_getig").toLocalDate();
                    LocalDate envio = resultSet.getDate("data_entrega").toLocalDate();
                    LocalDate acervo = resultSet.getDate("data_acervo").toLocalDate();
                    Cadastro cadastro = new Cadastro();
                    cadastro.setGerencia(map.get(gerencia));
                    cadastro.setId(id);
                    cadastro.setReferencia(referencia);
                    cadastro.setProcesso(processo);
                    cadastro.setInteressado(interessado);
                    cadastro.setGetig(getig);
                    cadastro.setEnvio(envio);
                    cadastro.setAcervo(acervo);
                    cadastros.add(cadastro);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        con.fecharConexao();
        return cadastros;

    }

    public List<Cadastro> filtrarMateriais(Filtro filtro, String texto) {
        Conexao con = new Conexao();

        String sql = "SELECT m.*, g.nome from materiais m, gerencia g WHERE m.id_gerencia = g.id ";

        if (filtro == Filtro.INTERESSADO) {
            sql += " AND m.interessado like '%"+ texto + "%'";
        } else if (filtro == Filtro.PROCESSO) {
            sql += " AND m.processo like '%"+ texto + "%'";
        } else if (filtro == Filtro.REFERENCIA) {
            sql += " AND m.referencia like '%"+ texto + "%'";
        } else if (filtro == Filtro.GERENCIA) {
            sql += " AND g.nome ilike '%"+ texto + "%'";
        }

        sql += " ORDER BY id";

        List<Cadastro> cadastros = new ArrayList<Cadastro>();

        List<Gerencia> gerencias = getGerencias();
        Map<Integer, Gerencia> map = new HashMap<>();

        for (Gerencia gerencia : gerencias) {
            map.put(gerencia.getId(), gerencia);
        }

        ResultSet resultSet = con.ExecutaSelect(sql);

        if (resultSet != null) {
            try {
                while (resultSet.next()) {
                    int id = resultSet.getInt("id");
                    String referencia = resultSet.getString("referencia");
                    String processo = resultSet.getString("processo");
                    String interessado = resultSet.getString("interessado");
                    int gerencia = resultSet.getInt("id_gerencia");
                    LocalDate getig = resultSet.getDate("data_getig").toLocalDate();
                    LocalDate envio = resultSet.getDate("data_entrega").toLocalDate();
                    LocalDate acervo = resultSet.getDate("data_acervo").toLocalDate();
                    int paginas = resultSet.getInt("paginas");
                    Cadastro cadastro = new Cadastro();
                    cadastro.setGerencia(map.get(gerencia));
                    cadastro.setId(id);
                    cadastro.setReferencia(referencia);
                    cadastro.setProcesso(processo);
                    cadastro.setInteressado(interessado);
                    cadastro.setGetig(getig);
                    cadastro.setEnvio(envio);
                    cadastro.setAcervo(acervo);
                    cadastro.setQuantidadePaginas(paginas);
                    cadastros.add(cadastro);
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        con.fecharConexao();
        return cadastros;
    }

}