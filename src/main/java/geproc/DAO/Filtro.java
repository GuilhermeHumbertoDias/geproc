package geproc.DAO;

/**
 * Created by Guilerme on 08/05/2017.
 */
public enum Filtro {

    REFERENCIA ("Referência"),
    PROCESSO ("Processo"),
    INTERESSADO ("Interessado"),
    GERENCIA ("Gerência");

    private final String descricao;

    private Filtro (String descricao) {
        this.descricao = descricao;
    }

    @Override
    public String toString() {
        return descricao;
    }
}
