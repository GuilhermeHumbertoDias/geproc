package geproc.DAO;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import geproc.view.Main;

import java.sql.*;


public class Conexao {
    private String url;
    private String usuario;
    private String senha;
    private Connection con;

    public Conexao() {
        this(Main.ip);
    }

    Conexao(String ip) {
        url = "jdbc:postgresql://"+ip+":5432/geproc";
        usuario = "geproc";
        senha = "geproc";
        /*usuario = "postgres";
        senha = "Guigo1997";*/


        try {
            Class.forName("org.postgresql.Driver");
            con = DriverManager.getConnection(url, usuario, senha);
            //System.out.printf("[BANCO DE DADOS] = CONEXÃO BEM SUCEDIDA\n");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("[BANCO DE DADOS] = ERRO NA CONEXÃO");
        }
    }

    public int ExecutaSQL(String sql) {

        try {
            Statement stm = con.createStatement();
            int res = stm.executeUpdate(sql);
            return res;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public ResultSet ExecutaSelect(String sql) {

        try {
            Statement stm = con.createStatement();
            ResultSet resultSet = stm.executeQuery(sql);
            return resultSet;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void fecharConexao() {

        try {
            con.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}