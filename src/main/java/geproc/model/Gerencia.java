package geproc.model;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

public class Gerencia {

    int id;
    String nome;

    public Gerencia() {
        this.id = id;
        this.nome = nome;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String toString() {
        return nome;
    }
}