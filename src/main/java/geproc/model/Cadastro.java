package geproc.model;

/*
 Desenvolvido por:
 Guilherme Humberto Dias -- IPREV -- 2017
*/

import geproc.model.Gerencia;

import java.time.LocalDate;

public class Cadastro {

    public int id;
    public Gerencia gerencia;
    public String referencia;
    public String processo;
    public String interessado;
    public LocalDate getig;
    public LocalDate envio;
    public LocalDate acervo;
    public int quantidadePaginas;


    public Cadastro() {
    }

    public String getReferencia() {
        return referencia;
    }

    public String setReferencia(String referencia) {
        this.referencia = referencia;
        return referencia;
    }

    public String getProcesso() {
        return processo;
    }

    public String setProcesso(String processo) {
        this.processo = processo;
        return processo;
    }

    public String getInteressado() {
        return interessado;
    }

    public String setInteressado(String interessado) {
        this.interessado = interessado;
        return interessado;
    }

    public LocalDate getGetig() {
        return getig;
    }

    public LocalDate setGetig(LocalDate getig) {
        this.getig = getig;
        return getig;
    }

    public LocalDate getEnvio() {
        return envio;
    }

    public LocalDate setEnvio(LocalDate envio) {
        this.envio = envio;
        return envio;
    }

    public LocalDate getAcervo() {
        return acervo;
    }

    public LocalDate setAcervo(LocalDate acervo) {
        this.acervo = acervo;
        return acervo;
    }

    public Gerencia getGerencia() {
        return gerencia;
    }

    public Gerencia setGerencia(Gerencia gerencia) {
        this.gerencia = gerencia;
        return gerencia;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getQuantidadePaginas() {
        return quantidadePaginas;
    }

    public void setQuantidadePaginas(int quantidadePaginas) {
        this.quantidadePaginas = quantidadePaginas;
    }
}